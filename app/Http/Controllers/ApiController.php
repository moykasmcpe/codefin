<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function index(){
        // Peticion a la API REST por metodo CURL GET
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.datos.gob.mx/v1/inai.solicitudes',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            // decodificamos el JSON
            $response = json_decode($response);
            $response = $response->results;
        //

        // Variables
            $Depen = [];            // Dependencia
            $TimeRequest = [];      // Tiempo de respuesta
            $MedioEntrega = [];     // Medio de entrega
            $suma_timestamps = 0;   // Suma de los timestamps
            $i = 0;                 // contador
        //


        // Reordenamos los resultados de la API en diferentes arrays
        foreach ($response as $key => $value) {
            $Depen[$value->DEPENDENCIA]['ESTADO_NAME'] = $value->ESTADO;
            $Depen[$value->DEPENDENCIA]['ESTADO'] = $i++;
            $Depen[$value->DEPENDENCIA]['NACIONAL_NAME'] = $value->PAIS;
            $Depen[$value->DEPENDENCIA]['NACIONAL'] = $i++;

            $unix = DateTime::createFromFormat("d/m/Y", $value->FECHARESPUESTA)->format('U');
            $TimeRequest[$key]['ESTADO']['PUNTOS'] = $i++;
            $TimeRequest[$key]['ESTADO']['NAME'] = $value->ESTADO;
            $TimeRequest[$key]['NACIONAL']['PUNTOS'] = $i++;
            $TimeRequest[$key]['NACIONAL']['NAME'] = $value->PAIS;
            $TimeRequest[$key]['unix'] = $unix;
            $TimeRequest[$key]['date'] = $value->FECHARESPUESTA;

            $MedioEntrega[$value->MEDIOENTREGA]['PUNTOS'] = $i++;
            $MedioEntrega[$value->MEDIOENTREGA]['RESPUESTA'] = $value->RESPUESTA;
            $MedioEntrega[$value->MEDIOENTREGA]['TEXTORESPUESTA'] = $value->TEXTORESPUESTA;
        }

        // Dependencia
            /*
                Enunciado:
                    1.- Cuál es la dependencia que recibe más solicitudes (a nivel nacional y por estado).

                Procedimiento:
                    Buscamos el valor más alto del array con la funcion max() de PHP, recorremos el array $Depen,
                    Y validamos si el valor es igual al valor más alto del array.
            */
            $max = max($Depen);
            foreach ($Depen as $key => $value) {
                if ($value['ESTADO'] == $max['ESTADO']) {
                    $data = [
                        'Dependencia' =>  $key,
                        'Estado' =>  $value['ESTADO'],
                        'Nacional' =>  $value['NACIONAL'],
                    ];
                }
            }

            // RESULTADO
            echo "======================================================================================================<br>
            Resultado de dependencias: <br> <pre>";
            print_r($data);
            echo "</pre>";
            echo "======================================================================================================<br>";
        //


        // Media, Mediana y Estandar

            /*
                Enunciado:
                    2.- Cuál es la media, mediana y desviación estándar para el tiempo de respuesta de las solicitudes (a nivel nacional y por estado).
            */

            //Media
                /*
                    Procedimiento:
                        Una vez que tenemos formateada la fecha en UNIX sumamos las fechas y dividimos por la cantidad de arrays
                */
                foreach ($TimeRequest as $key => $value) {
                    $suma_timestamps += $value['unix'];
                }

                $media = $suma_timestamps/count($TimeRequest);

                // RESULTADO
                echo "======================================================================================================<br><pre>";
                print_r('Resultado de la media: '.date('d/m/Y', $media));
                echo "</pre>";
                echo "======================================================================================================<br>";
            //

            //Mediana
                /*
                    Procedimiento:
                        Divibimos la cantidad de arrays en 2 y sacamos la mediana
                */
                $mediana = count($TimeRequest)/2;
                $mediana = $TimeRequest[$mediana];

                // RESULTADO
                echo "======================================================================================================<br>
                Resultado de la mediana: <pre>";
                print_r($mediana);
                echo "</pre>";
                echo "======================================================================================================<br>";
            //

        //

        // Medio de Entrega
            /*
                Enunciado:
                    3.- Qué medio de entrega considera, es más eficiente, justificar la respuesta

                Procedimiento:
                    Buscamos el valor más alto del array con la funcion max() de PHP, recorremos el array $MedioEntrega,
                    Y validamos si el valor es igual al valor más alto del array.
            */
            foreach ($MedioEntrega as $key => $value) {
                if ($value == max($MedioEntrega)) {
                    $DataEntrega = [
                        'MedioEntrega' =>  $key,
                        'Respuesta' =>  $value['RESPUESTA'],
                    ];
                }
            }

            // RESULTADO
            echo "======================================================================================================<br>
                Resultado de medio de entrega: <pre>";
            print_r($DataEntrega);
            echo "</pre>";
            echo "======================================================================================================<br>";
        //
    }
}
